﻿using System;
using System.IO;

namespace SerializedTest {
  public struct Vector3 {
    public float x;
    public float y;
    public float z;

    public Vector3(float x, float y, float z) {
      this.x = x;
      this.y = y;
      this.z = z;
    }
    public Vector3(BinaryReader br) {
      x = br.ReadSingle();
      y = br.ReadSingle();
      z = br.ReadSingle();
    }
    public Vector3(byte[] arg) {
      if (arg.Length != 12) {
        throw new ArgumentException("Very short array for a vector.", nameof(arg));
      }
      byte[] xMas = new byte[4];
      byte[] yMas = new byte[4];
      byte[] zMas = new byte[4];
      Array.Copy(arg, 0, xMas, 0, 4);
      Array.Copy(arg, 4, yMas, 0, 4);
      Array.Copy(arg, 8, zMas, 0, 4);

      x = BitConverter.ToSingle(xMas, 0);
      y = BitConverter.ToSingle(yMas, 0);
      z = BitConverter.ToSingle(zMas, 0);
    }
    public Vector3(byte[] arg, bool flag) {
      if (arg.Length != 12) {
        throw new ArgumentException("Very short array for a vector.", nameof(arg));
      }
      byte[] xMas = new byte[4];
      byte[] yMas = new byte[4];
      byte[] zMas = new byte[4];
      Buffer.BlockCopy(arg, 0, xMas, 0, 4);
      Buffer.BlockCopy(arg, 4, yMas, 0, 4);
      Buffer.BlockCopy(arg, 8, zMas, 0, 4);

      x = BitConverter.ToSingle(xMas, 0);
      y = BitConverter.ToSingle(yMas, 0);
      z = BitConverter.ToSingle(zMas, 0);
    }

    public byte[] Serialize() {
      byte[] result = new byte[12];
      BitConverter.GetBytes(x).CopyTo(result, 0);
      BitConverter.GetBytes(y).CopyTo(result, 4);
      BitConverter.GetBytes(z).CopyTo(result, 8);
      return result;
    }
  }
}
