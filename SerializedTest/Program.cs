﻿using System;
using System.Diagnostics;
using System.IO;

namespace SerializedTest
{
  class Program {
    private static Vector3[] vectors; 

    static void Main(string[] args) {
      var num = 0;
      if(args.Length <= 0 || !int.TryParse(args[0], out num)) return;

      vectors = new Vector3[num];
      var rnd = new Random();

      for (var i = 0; i < num; i++) {
        vectors[i] = new Vector3((float) rnd.NextDouble(), (float)rnd.NextDouble(), (float)rnd.NextDouble());
      }

      byte[] bytes = new byte[0];
      Vector3[] testVectors;

      var sw = new Stopwatch();

      sw.Start();
      for (var count = 0; count < 10; count++) {
        using (var ms = new MemoryStream()) {
          using (var bw = new BinaryWriter(ms)) {
            for (var i = 0; i < num; i++) {
              bw.Write(vectors[i]);
            }
          }
          bytes = ms.ToArray();
        }
      }
      sw.Stop();
      Console.WriteLine($"BinaryWriter - {sw.ElapsedTicks / 10}");

      sw.Reset();
      sw.Start();
      for (var count = 0; count < 10; count++) {
        testVectors = new Vector3[num];
        using (var ms = new MemoryStream(bytes)) {
          using (var br = new BinaryReader(ms)) {
            for (var i = 0; i < num; i++) {
              testVectors[i] = new Vector3(br);
            }
          }
        }
      }
      sw.Stop();
      Console.WriteLine($"BinaryReader - {sw.ElapsedTicks / 10}");
      
      sw.Reset();
      sw.Start();
      for (var count = 0; count < 10; count++) {
        for (var i = 0; i < num; i++) {
          var buf = vectors[i].Serialize();
          Array.Copy(buf, 0, bytes, i*4, 4);
        }
      }
      sw.Stop();
      Console.WriteLine($"BitConverter serialize Array copy - {sw.ElapsedTicks / 10}");

      sw.Reset();
      var buf1 = new byte[12];
      sw.Start();
      for (var count = 0; count < 10; count++) {
        testVectors = new Vector3[num];
        for (var i = 0; i < num; i++) {
          Array.Copy(bytes, i * 12, buf1, 0, 12);
          testVectors[i] = new Vector3(buf1);
        }
      }
      sw.Stop();
      Console.WriteLine($"BitConverter deserialize Array copy - {sw.ElapsedTicks / 10}");

      sw.Reset();
      sw.Start();
      for (var count = 0; count < 10; count++)
      {
        for (var i = 0; i < num; i++)
        {
          var buf = vectors[i].Serialize();
          Buffer.BlockCopy(buf, 0, bytes, i * 4, 4);
        }
      }
      sw.Stop();
      Console.WriteLine($"BitConverter serialize Buffer copy - {sw.ElapsedTicks / 10}");

      sw.Reset();
      sw.Start();
      for (var count = 0; count < 10; count++)
      {
        testVectors = new Vector3[num];
        for (var i = 0; i < num; i++)
        {
          Buffer.BlockCopy(bytes, i * 12, buf1, 0, 12);
          testVectors[i] = new Vector3(buf1, false);
        }
      }
      sw.Stop();
      Console.WriteLine($"BitConverter deserialize Buffer copy - {sw.ElapsedTicks / 10}");
    }
  }
}
