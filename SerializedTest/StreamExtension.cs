﻿using System.IO;

namespace SerializedTest {
  public static class StreamExtension {
    public static void Write(this BinaryWriter bw, Vector3 vec) {
      bw.Write(vec.x);
      bw.Write(vec.y);
      bw.Write(vec.z);
    }

    public static Vector3 ReadVector3(this BinaryReader br) {
      return new Vector3(br.ReadSingle(), br.ReadSingle(), br.ReadSingle());
    }
  }
}